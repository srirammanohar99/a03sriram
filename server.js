var path = require("path");
var express = require("express");
var logger = require("morgan");

var bodyParser = require("body-parser"); // simplifies access to request body


var app = express();  // make express app

app.use(bodyParser.urlencoded({extended: false }));
app.use(express.static(path.join(__dirname + '/views')));
app.use(express.static('assets'));
// 1 set up the view engine
app.set('view engine', 'ejs');


var entries = [];
app.locals.entries = entries; // now entries can be accessed in .ejs files

// 3 set up an http request logger to log every request automagically
app.use(logger("dev"));     // app.use() establishes middleware functions
app.use(bodyParser.urlencoded({ extended: false }));
app.use("/assets",express.static(__dirname + "/assets"));


// 4 handle http GET requests (default & /new-entry)
app.get("/guestbook", function (request, response) {
  response.render("index");
});

app.get("/", function (request, response) {
  response.render("hom3");
});

app.get("/hom3", function (request, response) {
  response.render("hom3");
});

app.get("/new-entry", function (request, response) {
  response.render("new-entry");
});

app.get("/index", function (request, response) {
  response.render("index");
});
// 5 handle an http POST request to the new-entry URI 
app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({  // store it
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("/index");  // where to go next? Let's go to the home page :)
});




app.get("/", function (request, response) {
  response.render("hom3");
});


app.get("/contact", function (request, response) {
  response.render("contact");
});

app.get("/userinput", function (request, response) {
  response.render("userinput");
});

app.get("/test", function (request, response) {
  response.render("test");
});



 
app.use(function (request, response) {
  response.status(404).render("404");
});

// Listen for an application request on port 8081 & notify the developer
app.listen(8081, function () {
  console.log('Example app listening on port 8081!')
});